<?php defined('BASEPATH') OR exit('No direct script access allowed');

require(APPPATH.'/libraries/REST_Controller.php');

/**
 * REST API
 */
class XOapi extends REST_Controller {
    
    /**
     * Loads database.
     * @controller
     */
    function __construct(){
        parent::__construct();
        header('Access-Control-Allow-Origin: *');
        
        $this->load->database();
    }
    
    /**
     * Function which gets all usernames and their online status from the database.
     * @access public
     */
    public function users_get() {
        
        $sql = "SELECT username, online FROM `users`";
        $query = $this->db->query($sql);
        $users = $query->result();
         
        if($users)
        {
            $this->response($users, 200);
        }
 
        else
        {
            $this->response(NULL, 404);
        }
    }
    
    /**
     * Function which gets all usernames that are online from the database.
     * @access public
     */
    public function usersonline_get(){
        $sql = "SELECT username FROM `users` WHERE online = '1'";
        $query = $this->db->query($sql);
        $users = $query->result();
        
        $this->response(json_encode($users), 200);
    }
    
    /**
     * Function which checks if a specific username exists in the database.
     * @access public
     */
    public function user_get($username){
        $sql = "SELECT IF (EXISTS(SELECT * FROM users WHERE username = ?),1,0) AS result";
        $query = $this->db->query($sql, $username);
        $result = $query->row()->result;
        
        if($result == 1){
            $this->response(FALSE);
        }else{
            $this->response(TRUE);
        }
    }
    
    /**
     * Function which marks users status to online.
     * It gets the username in POST parameter.
     * @access public
     */
    public function useronline_post(){
        
        $result = array(
            'username' => $this->post('username')
        );
        
        $username = $result['username'];
        
        $sql = "SELECT online FROM users where username= ?";
        $query = $this->db->query($sql, $username);
        $result = $query->row()->online;
        
        if($result == 1){
            $this->response($username);
        }
    }
    
    /**
     * Function which adds a new user and its password to the database.
     * It gets the username and password in POST parameters.
     * @access public
     */
    public function user_post(){
        
        $result = array(
            'username' => $this->post('username'),
            'password' => $this->post('password')
        );
        
        $username = $result['username'];
        $password = $result['password'];
        
        if($username != null && $password != null){
            
            $sql = "SELECT IF (EXISTS(SELECT * FROM users WHERE username = ?),1,0) AS result";
            $query = $this->db->query($sql, $username);
            $result = $query->row()->result;
            
            if($result == 0){
                $sql = "INSERT INTO users (username, password)
                VALUES (?, ?)";
                $query = $this->db->query($sql, array($username, $password));
                $this->response(TRUE);
            }else{
                $this->response(FALSE);
            }
        }
    }
    
    /**
     * Function which checks if it is allowed to log in with the specific username and password combination.
     * It gets the username and password in POST parameters.
     * @access public
     */
    public function userlog_post(){
        
        $result = array(
            'username' => $this->post('username'),
            'password' => $this->post('password')
        );
        
        $username = $result['username'];
        $password = $result['password'];
        
        if($username != null && $password != null){
            
            $sql = "SELECT IF (EXISTS(SELECT * FROM users WHERE username = ?),1,0) AS result";
            $query = $this->db->query($sql, $username);
            $resultexists = $query->row()->result;
            
            if($resultexists == 1){
                $sql = "SELECT online FROM users WHERE username = ?";
                $query = $this->db->query($sql, $username);
                $resultonline = $query->row()->online;
                
                if($resultonline == 0){
                    $sql = "SELECT password FROM users WHERE username = ?";
                    $query = $this->db->query($sql, $username);
                    $resultpassmatch = $query->row()->password;
                
                    if($resultpassmatch == $password){
                        $sql = "UPDATE users SET online=1 WHERE username = ?";
                        $query = $this->db->query($sql, $username);
                        $this->response(TRUE);
                    }
                }
            }
        }
        
        $this->response(FALSE);
    }
    
    /**
     * Function which logs out a specific user.
     * It gets the username in POST parameter.
     * @access public
     */
    public function userlogout_post(){
        
        $result = array(
            'username' => $this->post('username')
        );
        
        $username = $result['username'];
        
        if($username != null){
            
            $sql = "SELECT IF (EXISTS(SELECT * FROM users WHERE username = ?),1,0) AS result";
            $query = $this->db->query($sql, $username);
            $resultexists = $query->row()->result;
            if($resultexists == 1){
                $sql = "SELECT online FROM users WHERE username = ?";
                $query = $this->db->query($sql, $username);
                $resultonline = $query->row()->online;
                
                if($resultonline == 1){
                    $sql = "UPDATE users SET online=0 WHERE username = ?";
                    $query = $this->db->query($sql, $username);
                    $this->response(TRUE);
                }
            }
        }
        
        $this->response(FALSE);
    }
    
    /**
     * Function which logs out every user that is logged in.
     * @access public
     */
    public function userslogout_get(){
        $sql = "UPDATE users SET online=0";
        $query = $this->db->query($sql);
        
        $sql = "SELECT IF (EXISTS(SELECT * FROM users WHERE online = 1),1,0) AS result";
            $query = $this->db->query($sql);
            $result = $query->row()->result;
            if($result == 0){
                $this->response(TRUE);
            }else{
                $this->response(FALSE);
            }
    }
        
    
    public function users_delete(){
        
    }
    
    public function user_delete($username){
        
    }
    
    
    /**
     * Function which gets all the messages from the database and deletes oldest
     * messages if there is more than 50 messages. 
     * The message count should always be 50.
     * @access public
     */
    public function messages_get() {
        
        $sql = "SELECT COUNT(*) FROM `messages`";
        $query = $this->db->query($sql);
        $result = $query->row_array();
        $count = $result['COUNT(*)'];
        $row = $count - 50;
        
        if ($count > 50){
            $sql = "DELETE FROM `messages` WHERE id between 1 AND ?";
            $query = $this->db->query($sql, $row);
            $sql = "UPDATE messages SET id=id-?";
            $query = $this->db->query($sql, $row);
        }
        
        $sql = "SELECT * FROM `messages`";
        $query = $this->db->query($sql);
        $messages = $query->result();
         
        if($messages)
        {
            $this->response(json_encode($messages), 200);
        }
 
        else
        {
            $this->response(NULL, 404);
        }
    }
    
    public function message_get() {
        
    }
    
    /**
     * Function which posts new message and its sender to the database.
     * It also counts the next id for the message (for keeping it at 50 at all times).
     * @access public
     */
    public function message_post() {
        
        $sql = "SELECT COUNT(*) FROM `messages`";
        $query = $this->db->query($sql);
        $result = $query->row_array();
        $count = $result['COUNT(*)'];
        
        $id = $count +1;
        
        $result = array(
            'username' => $this->post('username'),
            'message' => $this->post('message')
        );
        
        $username = $result['username'];
        $message = $result['message'];
        
        if($id != null && $username != null && $message != null){
            $sql = "INSERT INTO messages (id, sender, message)
            VALUES (?, ?, ?)";
            $query = $this->db->query($sql, array($id, $username, $message));
            $this->response($username);
        }
        
    }
    
    
}
?>